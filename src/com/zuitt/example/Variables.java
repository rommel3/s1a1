// package in java is used to group related classes
// follows reverse name notation
package com.zuitt.example;

public class Variables {

    public static void main(String[] arg){
        // variable
        //data type identifierno
        // initial value for char is null and int is 0
        int age;
        char middleInitial;

        // variable decleration
        int x;
        int y = 0;

        // initialisation
        x = 1;
        y = 5;

        System.out.println("The value of y is" + " " + y + " " + "and the value of x is" + " " + x);

        //primitive data types

        // int - whole number
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long - whole number with long digits
        //L is added in the number to recognise as long
        long worldPopulation = 80000000L;
        System.out.println(worldPopulation);

        //float - number with decimal
        float piFloat = 3.1415979998798789F;
        System.out.println(piFloat);

        //double - number with long decimal
        double piDFloat = 3.1415979998798789D;
        System.out.println(piDFloat);

        //char - single character
        //uses single qoute
        char letter = 'a';
        System.out.println(letter);

        //boolean - 1 or 0
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //constants - constant value
        //java uses "final" keyword. The value cannot be changed.
        final int PRINCIPLE = 3000;
        System.out.println(PRINCIPLE);

        //non primitive data type
            // also known as reference data types refers to instance or objects
            // do not directly store

        //examples
        // string - stores a sequence or array of characters
        String userName = "rommel";
        System.out.println(userName);


    }
}
