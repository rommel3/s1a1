package com.zuitt.example;

import java.util.Scanner;

public class s1a1 {

    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("First Name: ");
        // first name
        String firstName = userInput.nextLine();
        System.out.println(firstName);
        // last name
        System.out.println("Last Name: ");
        String lastName = userInput.nextLine();
        System.out.println(lastName);
        //first grade
        System.out.println("First Subject Grade: ");
        double firstGrade = Double.parseDouble(userInput.nextLine());
        System.out.println(firstGrade);
        //2nd grade
        System.out.println("Second Subject Grade: ");
        double secondGrade = Double.parseDouble(userInput.nextLine());
        System.out.println(secondGrade);
        //third grade
        System.out.println("Third Subject Grade: ");
        double thirdGrade = Double.parseDouble(userInput.nextLine());
        System.out.println(thirdGrade);

        System.out.println("Good day," + " " + firstName + " " + lastName + "." );
        System.out.println("Your average grade is:" + " " + (firstGrade + secondGrade + thirdGrade) / 3);
    }
}
