package com.zuitt.example;

import java.util.Scanner;

public class TypeConversation {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.print("how old are you?");

        double age = Double.parseDouble(userInput.nextLine());
        System.out.println(age);


        // other method
        double age2 = userInput.nextDouble();
        System.out.println(age2);

        System.out.println("enter 1st number");
        int num1 = userInput.nextInt();

        System.out.println("enter the 2nd number");
        int num2 = userInput.nextInt();

        System.out.println(num1 + num2);
    }
}
